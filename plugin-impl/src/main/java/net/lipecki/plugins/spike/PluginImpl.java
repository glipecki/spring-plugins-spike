package net.lipecki.plugins.spike;

import net.lipecki.spikes.plugins.api.Plugin;
import net.lipecki.spikes.plugins.api.SampleService;

public class PluginImpl implements Plugin {

	public PluginImpl(final SampleService service) {
		System.out.println("Got!" + service);
	}

	public String getName() {
		return "PluginImpl-external";
	}

}
