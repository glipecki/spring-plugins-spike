package net.lipecki.spikes.plugins;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import net.lipecki.spikes.plugins.api.Plugin;

import org.springframework.context.ApplicationContext;

public class PluginManager {

	public PluginManager(final ApplicationContext applicationContext) {
		getPluginsMap(applicationContext);
		System.out.println("Plugins: " + pluginsCount());
	}

	private void getPluginsMap(final ApplicationContext applicationContext) {
		final Map<String, Plugin> beans = applicationContext.getBeansOfType(Plugin.class);
		for (final Entry<String, Plugin> e : beans.entrySet()) {
			plugins.put(e.getValue().getName(), e.getValue());
		}
	}

	public int pluginsCount() {
		return plugins.size();
	}

	private final ConcurrentHashMap<String, Plugin> plugins = new ConcurrentHashMap<String, Plugin>();

}
