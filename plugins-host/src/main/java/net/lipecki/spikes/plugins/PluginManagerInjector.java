package net.lipecki.spikes.plugins;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(value = "classpath*:pluginCtx.xml")
public class PluginManagerInjector {

	@Bean
	public PluginManager getPluginManager() {
		return new PluginManager(applicationContext);
	}

	@Autowired
	ApplicationContext applicationContext;

}
