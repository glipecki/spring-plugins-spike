package net.lipecki.spikes.plugins;

import net.lipecki.spikes.plugins.api.SampleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class RestConfiguration {

	@Bean
	public SampleController getVersionController() {
		return new SampleController(pluginManagerInjector.getPluginManager());
	}

	@Bean(name = "sampleService")
	public SampleService getSampleService() {
		return new SampleService() {
		};
	}

	@Autowired
	PluginManagerInjector pluginManagerInjector;

}
