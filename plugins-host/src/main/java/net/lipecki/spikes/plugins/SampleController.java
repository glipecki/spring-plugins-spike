package net.lipecki.spikes.plugins;

import java.io.IOException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping(value = "/test")
public class SampleController {

	public SampleController(final PluginManager pluginManager) {
		this.pluginManager = pluginManager;
	}

	@RequestMapping("")
	@ResponseBody
	public String beansList() throws IOException {
		return "Available plugins: " + pluginManager.pluginsCount();
	}

	private final PluginManager pluginManager;
}
